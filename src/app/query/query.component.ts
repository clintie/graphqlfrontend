import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import gql from 'graphql-tag';
import { log } from 'util';



@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.css']
})
export class QueryComponent implements OnInit {
  quote: Observable<any>;
  constructor(private apollo: Apollo) { }

  ngOnInit() {
    this.quote = this.apollo.watchQuery<any>({
      query: gql`
      
      {
          #comment! :)
          quoteOfTheDay

        }
      
      `
        
    }).valueChanges.pipe(
        map(result => result.data)
    );

    this.quote.subscribe((r)=>{console.log(r)})
  };



}
